# UNSW-NB15-preprocessing
Preprocessing [UNSW-NB15](https://research.unsw.edu.au/projects/unsw-nb15-dataset) dataset for anomaly detection using LSTM.

for GPU support: 
```sh
CUDA 
cuDNN
```
\
cuDNN installation: 
```sh
Copy files from [installpath]\cuda\bin\ to C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v[version]\bin.
Copy files from [installpath]\cuda\include\ to C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v[version]\include.
Copy files from [installpath]\cuda\lib\ to C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v[version]\lib.
```
