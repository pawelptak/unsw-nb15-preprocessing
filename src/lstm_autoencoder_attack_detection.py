import matplotlib.pyplot as plt

from settings import ATTACK_CATEGORIES
from preprocessing import *
import pandas as pd
from sklearn.decomposition import PCA
from keras.models import load_model
import seaborn as sns
from sklearn.metrics import confusion_matrix
from keras.layers import LSTM, Dense, Input, RepeatVector, TimeDistributed
from keras.models import Model
from keras.callbacks import EarlyStopping
from keras import regularizers

def autoencoder_model(X):
    num_sequences = X.shape[1]
    num_features = X.shape[2]
    inputs = Input(shape=(num_sequences, num_features))
    L1 = LSTM(16, activation='relu', return_sequences=True,
              kernel_regularizer=regularizers.l2(0.00))(inputs)
    L2 = LSTM(4, activation='relu', return_sequences=False)(L1)
    L3 = RepeatVector(X.shape[1])(L2)
    L4 = LSTM(4, activation='relu', return_sequences=True)(L3)
    L5 = LSTM(16, activation='relu', return_sequences=True)(L4)
    output = TimeDistributed(Dense(X.shape[2]))(L5)
    model = Model(inputs=inputs, outputs=output)
    return model


def fit_model(model, X_train, y_train, epochs, batch_size, val_split, plot=True):
    callback = EarlyStopping(monitor='loss', patience=3)
    history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size,
                        validation_split=val_split, shuffle=False, callbacks=[callback]).history

    if plot:
        # plot the training losses
        fig, ax = plt.subplots(figsize=(14, 6), dpi=80)
        ax.plot(history['loss'], 'b', label='Train', linewidth=2)
        ax.plot(history['val_loss'], 'r', label='Validation', linewidth=2)
        ax.set_title('Model loss', fontsize=16)
        ax.set_ylabel('Loss (mae)')
        ax.set_xlabel('Epoch')
        ax.legend(loc='upper right')
        plt.show()

    model.save('lstm_model.h5')


# https://www.mikulskibartosz.name/pca-how-to-choose-the-number-of-components/
def get_optimal_pca_components(data, attack_cat, variance=.65, plot=False):
    pca = PCA()
    pca.fit(data)
    y = np.cumsum(pca.explained_variance_ratio_)
    if plot:
        plt.plot(y, marker='o', linestyle='--', color='b')
        plt.xlabel('Number of components')
        plt.ylabel('Cumulative variance (%)')
        plt.axhline(y=variance, color='r', linestyle='-')
        plt.title(attack_cat)
        plt.show()
    optimal = np.where(y >= variance)[0][0]
    if optimal > 300:
        optimal = 300
    return optimal



all_data = pd.read_pickle(PROCESSED_DATA_PATH + '_pckl')

# for cat in ATTACK_CATEGORIES:
#     attack_data = all_data[all_data['attack_cat'] == cat]
#     attack_data.pop('attack_cat')
#     attack_data.pop('Label')
#     attack_data = np.array(attack_data)
#     get_optimal_pca_components(attack_data, cat, plot=True)


variances = [0.65, 0.55, 0.65, 0.75, 0.8, 0.55, 0.6, 0.95, .8]

for attack_name, var in zip(ATTACK_CATEGORIES, variances):
    attack_data = all_data[all_data['attack_cat'] == attack_name]
    train_percent = .8  # train data percentage
    train_size = int(train_percent * len(attack_data))
    train_data = attack_data[:train_size]
    normal_data = all_data[all_data['attack_cat'] == 'Normal']

    test_data = pd.concat([attack_data[train_size:], normal_data])

    generate_class_plot(train_data, 'attack_cat')
    generate_class_plot(test_data, 'attack_cat')

    train_data_labels = train_data.pop('Label')  # remove label from data
    train_data_categories = train_data.pop('attack_cat')
    test_data_labels = test_data.pop('Label')  # remove label from data
    test_data_categories = test_data.pop('attack_cat')
    print(f"Train data size: {len(train_data)}. Test data size: {len(test_data)}")


    train = np.array(train_data)
    pca_components = get_optimal_pca_components(train, attack_name, variance=var, plot=True)
    pca = PCA(n_components=pca_components)
    print(f"PCA reduction from {len(all_data.columns)} features to {pca_components}.")

    train = pca.fit_transform(train)

    test = np.array(test_data)
    test = pca.fit_transform(test)


    # reshape inputs for LSTM [samples, timesteps, features]
    X_train = train.reshape((train.shape[0], 1, train.shape[1]))
    X_test = test.reshape((test.shape[0], 1, test.shape[1]))

    model = autoencoder_model(X_train)
    model.compile(optimizer='adam', loss='mae')

    # fit the model to the data
    fit_model(model, X_train, X_train, epochs=500, batch_size=32, val_split=0.05)

    model = load_model('lstm_model.h5')

    predicted = model.predict(X_train)
    train_mae_loss = np.mean(np.abs(predicted - X_train), axis=tuple(range(1, X_train.ndim)))
    train_score_df = pd.DataFrame()
    train_score_df['loss'] = train_mae_loss
    plt.title('Loss Distribution', fontsize=16)
    sns.distplot(train_score_df['loss'], kde=True)
    plt.show()

    threshold = max(train_mae_loss)

    predicted = model.predict(X_test)
    test_mae_loss = np.mean(np.abs(predicted - X_test), axis=tuple(range(1, X_test.ndim)))
    test_score_df = test_data.copy()
    test_score_df['loss'] = test_mae_loss
    test_score_df['threshold'] = threshold
    test_score_df['anomaly'] = (test_score_df['loss'] > test_score_df['threshold']) * 1
    test_score_df['actual value'] = np.logical_xor(test_data_labels, 1).astype(int)
    test_score_df['category'] = test_data_categories
    # test_score_df.to_csv(f'predictions.csv', index=False)


    anomalies_truth = sum(test_score_df['actual value'] != 0)
    print('Actual anomalies:', anomalies_truth)
    differences = test_score_df['anomaly'].compare(test_score_df['actual value'])
    # print(f"Mistakes: {len(differences['other'].values)}, lines: {differences['other'].index.to_numpy()}.")

    conf_matrix = confusion_matrix(test_score_df['actual value'], test_score_df['anomaly'])
    sns.set(rc={'figure.figsize':(10, 8)})
    ax = sns.heatmap(conf_matrix, annot=True, cmap='Blues', fmt='g')
    ax.set_xlabel('\nPredicted Values')
    ax.set_ylabel('Actual Values ')

    ## Ticket labels - List must be in alphabetical order
    ax.xaxis.set_ticklabels(['No anomaly', 'Anomaly'])
    ax.yaxis.set_ticklabels(['No anomaly', 'Anomaly'])

    plt.title(attack_name)

    plt.show()

########################
# Experimental results (data rows=3000):
# Reconnaissance:
# pca 20 components, thres= 0.25, variance=.65, 788/1000 detected
# no pca, thres: auto, variance=.65, 236/1000 detected

# Generic:
# pca 19 components, thres= 0.3, variance=.8, 452/1000 detected

# Shellcode:
# pca 99 components, thres= auto, variance=.8, 26/1000 detected
# pca 13 components, thres= auto, variance=.55, 148/1000 detected

# Fuzzers:
# pca 13 components, thres= auto, variance=.55, 14/1000 detected
# pca 13 components, thres= .35, variance=.55, 364/1000 detected

# Worms:
# pca 20 components, thres= auto, variance=.99, 37/1000 detected

# Analysis:
# pca 152 components, thres= auto, variance=.95, 91/1000 detected
# pca 152 components, thres= .08, variance=.95, 343/1000 detected
# pca 152 components, thres= .08, variance=.65, 999/1000 detected

