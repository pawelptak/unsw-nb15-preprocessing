import pandas as pd
import tensorflow.python.framework.test_util
from keras.models import load_model
from settings import PROCESSED_DATA_PATH
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns

def gen_sequences_prediction(data, targets, seq_length):
    data_array = data.values
    num_elements = data_array.shape[0]
    data_sequences = []
    sequence_targets = []
    for start, stop in zip(range(0, num_elements-seq_length), range(seq_length, num_elements)):
        data_sequences.append(data_array[start:stop, :])
        sequence_targets.append(targets[stop])
        # print(data_array[start:stop, :])
        # print('NEXT LABEL', sequence_targets[stop])
    return np.array(data_sequences), np.array(sequence_targets)

def gen_sequences_reconstruction(data, seq_length):
    data_array = data.values
    num_elements = data_array.shape[0]
    data_sequences = []
    sequence_targets = []
    indexes = []
    for start, stop in zip(range(0, num_elements-seq_length), range(seq_length, num_elements)):
        indexes.append([start, stop])
        data_sequences.append(data_array[start:stop, :])
        sequence_targets.append(data_array[stop])
    return np.array(data_sequences), np.array(sequence_targets)

def fit_model(model, X_train, y_train, epochs, batch_size, val_split, plot=True):
    history = model.fit(X_train, y_train, epochs=epochs, batch_size=batch_size,
                        validation_split=val_split, shuffle=False).history

    if plot:
        # plot the training losses
        fig, ax = plt.subplots(figsize=(14, 6), dpi=80)
        ax.plot(history['loss'], 'b', label='Train', linewidth=2)
        ax.plot(history['val_loss'], 'r', label='Validation', linewidth=2)
        ax.set_title('Model loss', fontsize=16)
        ax.set_ylabel('Loss (mae)')
        ax.set_xlabel('Epoch')
        ax.legend(loc='upper right')
        plt.show()

    model.save('lstm_model.h5')

all_data = pd.read_csv(PROCESSED_DATA_PATH)

normal_data = all_data.loc[all_data['Label'] == 0] # get normal data (no attacks)
attack_data = all_data.loc[all_data['Label'] == 1]
attack_data_labels = attack_data.pop('Label') # remove label from data
normal_data_labels = normal_data.pop('Label') # remove label from data

attack_data = attack_data[['dur', 'sbytes']] # for testing
normal_data = normal_data[['dur', 'sbytes']] # for testing

# train_size = int(len(all_features) * 0.7)  # 70 % for training
# test_size = len(all_features) - train_size
#
# train, test = all_features.iloc[0:train_size], all_features.iloc[train_size:len(all_features)]
train = np.array(normal_data)
test = np.array(attack_data)
print(train.shape)
time_step = 2

X_train = train.reshape((train.shape[0], 1, train.shape[1]))
X_test = test.reshape((test.shape[0], 1, test.shape[1]))
# X_train, y_train = gen_sequences_reconstruction(train, time_step)
# X_test, y_test = gen_sequences_reconstruction(test, time_step)

# print('TRAIN X', X_train)
# print('TRAIN y', y_train)


# X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# # reshape inputs for LSTM [samples, timesteps, features]

#X_train = X_train.reshape((X_train.shape[0], time_step, X_train.shape[1]))
#X_test = X_test.reshape((X_test.shape[0], time_step, X_test.shape[1]))
print("Train data X shape:", X_train.shape)
# print("Train data y shape:", y_train.shape)
# print("Test data shape:", X_test.shape)

from keras import regularizers
from keras.models import Model
from keras.layers import LSTM, Dense, Input, RepeatVector, TimeDistributed

def autoencoder_model(X):
    num_sequences = X.shape[1]
    num_features = X.shape[2]
    inputs = Input(shape=(num_sequences, num_features))
    L1 = LSTM(16, activation='relu', return_sequences=True,
              kernel_regularizer=regularizers.l2(0.00))(inputs)
    L2 = LSTM(4, activation='relu', return_sequences=False)(L1)
    L3 = RepeatVector(X.shape[1])(L2)
    L4 = LSTM(4, activation='relu', return_sequences=True)(L3)
    L5 = LSTM(16, activation='relu', return_sequences=True)(L4)
    output = TimeDistributed(Dense(X.shape[2]))(L5)
    model = Model(inputs=inputs, outputs=output)
    return model
#
model = autoencoder_model(X_train)
model.compile(optimizer='adam', loss='mae')
model.summary()

# fit the model to the data
# fit_model(model, X_train, X_train, epochs=100, batch_size=32, val_split=0.05)

model = load_model('lstm_model.h5')

predicted = model.predict(X_train)
train_mae_loss = np.mean(np.abs(predicted - X_train), axis=(1, 2))

# train_score_df = pd.DataFrame(index=train[time_step:].index)
train_score_df = pd.DataFrame()
train_score_df['loss'] = train_mae_loss
plt.figure(figsize=(16,9), dpi=80)
plt.title('Loss Distribution', fontsize=16)
sns.distplot(train_score_df['loss'], kde=True, color='blue')
plt.show()

threshold = 0.05

predicted = model.predict(X_test)
test_mae_loss = np.mean(np.abs(predicted - X_test), axis=(1, 2))
test_score_df = attack_data
test_score_df['loss'] = test_mae_loss
test_score_df['threshold'] = threshold
test_score_df['anomaly'] = (test_score_df['loss'] > test_score_df['threshold']) * 1
test_score_df['acutal value'] = attack_data_labels
print(test_score_df)
#
# test_data_2 = gen_sequences_reconstruction(all_data[['dur', 'sbytes']], time_step)[0]
# print(test_data_2)
# predicted = model.predict(test_data_2)
# test_mae_loss = np.mean(np.abs(predicted - test_data_2), axis=(1, 2))
# test_score_df = pd.DataFrame()
# test_score_df['loss'] = test_mae_loss
# test_score_df['threshold'] = threshold
# test_score_df['anomaly'] = test_score_df['loss'] > test_score_df['threshold']
# print(test_score_df)



# # test_score_df['threshold'] = 0.65
# # test_score_df['anomaly'] = test_score_df.loss > test_score_df.threshold
# # test_score_df['dur'] = test[time_step:]['dur']
# # test_score_df['sbytes'] = test[time_step:]['sbytes']
#
# print(test_score_df)




# def flatten(X):
#     flattened_X = np.empty((X.shape[0], X.shape[2]))  # sample x features array.
#     for i in range(X.shape[0]):
#         flattened_X[i] = X[i, (X.shape[1] - 1), :]
#     return (flattened_X)

# plt.figure()
# plt.title("Actual Test Signal w/Anomalies")
# plt.plot(y_train[:len(y_train)], 'b')
#
# plt.figure()
# plt.title("Squared Error")
# mse = np.mean(np.power(flatten(X_test) - flatten(predicted), 2), axis=1)
# plt.plot(mse, 'r')
# mse_train = np.mean(np.power(flatten(X_train) - flatten(predicted_train), 2), axis=1)
#
# plt.figure()
# plt.title("Actual Test Signal with Anomalies")
# plt.plot(y_test[:len(y_test)], 'b')
# plt.savefig('Epoch.png', dpi=1200)
# plt.show()
#
#






