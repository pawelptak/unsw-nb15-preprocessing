DATA_PATH = 'network_data/raw/UNSW-NB15_1.csv'
FEATURES_PATH = 'network_data/raw/NUSW-NB15_features.csv'
LABEL_ENCODING_COLUMN_NAMES = ['srcip', 'dstip', 'proto', 'state', 'service', 'Label']
ONE_HOT_ENCODING_COLUMN_NAMES = ['proto', 'state', 'service',
                                 'is_sm_ips_ports', 'ct_state_ttl', 'is_ftp_login', 'srcip', 'dstip', 'stcpb', 'dtcpb', 'sport', 'dsport']
NORMALIZATION_COLUMN_NAMES = ['dur', 'Spkts', 'Dpkts', 'sbytes', 'dbytes', 'sttl', 'dttl', 'sloss', 'dloss', 'Sload',
                              'Dload', 'swin', 'dwin', 'smeansz', 'dmeansz', 'trans_depth', 'res_bdy_len', 'Sjit',
                              'Djit', 'Stime', 'Ltime', 'Sintpkt', 'Dintpkt', 'tcprtt', 'synack', 'ackdat',
                              'ct_flw_http_mthd', 'ct_ftp_cmd', 'ct_srv_src', 'ct_srv_dst', 'ct_dst_ltm',
                              'ct_src_ ltm', 'ct_src_dport_ltm', 'ct_dst_sport_ltm', 'ct_dst_src_ltm', ]
PROCESSED_DATA_PATH = './network_data/processed/all_processed.csv'
ATTACK_CATEGORIES = ['Normal', 'Exploits', 'Reconnaissance', 'DoS', 'Generic', 'Shellcode', ' Fuzzers', 'Worms', 'Backdoors', 'Analysis']