import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder, MinMaxScaler, OneHotEncoder
import numpy as np
from numpy import log
import seaborn as sn
from settings import *

def read_features(file_path):
    df = pd.read_csv(file_path, encoding='ansi', delimiter=',')
    return df['Name'].tolist()


def read_datatypes(file_path):
    df = pd.read_csv(file_path, encoding='ansi', delimiter=',')
    data_types = df['Type '].tolist()
    data_types = [t.replace('nominal', 'string') for t in data_types]
    data_types = [t.replace('Integer', 'int') for t in data_types]
    data_types = [t.replace('integer', 'string') for t in data_types]
    data_types = [t.replace('Float', 'float') for t in data_types]
    data_types = [t.replace('Timestamp', 'str') for t in data_types]  # to be changed
    data_types = [t.replace('Binary', 'bool') for t in data_types]
    data_types = [t.replace('binary', 'bool') for t in data_types]
    return data_types


def load_data(file_path, features_path):
    columns = read_features(features_path)
    data_types = read_datatypes(features_path)
    zip_iterator = zip(columns, data_types)
    types_dict = dict(zip_iterator)
    # print(types_dict)

    # nrows just for test purposes
    return pd.read_csv(file_path, names=columns, dtype=types_dict)


def process_missing_values(df):
    print('Missing values:')
    print(df.isna().sum())
    df = df.fillna('Normal')
    return df


def print_categories(df):
    for col_name in df.columns:
        unique_cat = len(df[col_name].unique())
        print(f"Feature '{col_name}' has {unique_cat} categories")


def label_encoding(df, col_names):
    le = LabelEncoder()
    for col_name in col_names:
        df[col_name] = le.fit_transform(df[col_name].values)
    return df


def one_hot_encoding(df, col_names):
    df = pd.get_dummies(df, columns=col_names, dtype=float, sparse=True)
    return df


def scale_data(df, col_names):
    min_max_scaler = MinMaxScaler()
    df[[f"{column}" for column in col_names]] = min_max_scaler.fit_transform(df[[f"{column}" for column in col_names]])


def generate_class_plot(df, col_name):
    column = df[col_name]
    classes_list = column.unique()
    print(classes_list)
    y = []
    for c in classes_list:
        y.append(column.value_counts()[c])

    n = len(df.index)
    k = len(df)

    h = -sum([(count / n) * log((count / n)) for count in y])  # shannon entropy

    print('Balance:', h / log(k))

    for i in range(len(classes_list)):  # show values on bars
        plt.text(classes_list[i], y[i] + 1, y[i])

    plt.bar(classes_list, y)
    plt.xticks(classes_list)
    plt.title("{:.2f}".format(h / log(k)))
    plt.xlabel(col_name)
    plt.ylabel("Count")
    plt.show()


def drop_highly_correlated_columns(df):
    corrMatrix = df.corr().abs()
    upper = corrMatrix.where(np.triu(np.ones(corrMatrix.shape), k=1).astype(bool))
    to_drop = [column for column in upper.columns if any(upper[column] > 0.95)]
    print(len(to_drop), 'to drop', to_drop)
    df.drop(to_drop, axis=1, inplace=True)
    sn.heatmap(corrMatrix)
    plt.show()


def sklearn_one_hot_encoding(df, col_names):
    enc = OneHotEncoder(sparse=False)

    for col_name in col_names:
        columns_encoded = enc.fit_transform(df[col_name].to_numpy().reshape(-1, 1))
        # print(columns_encoded)
        encoded_df = pd.DataFrame(columns_encoded, columns=enc.categories_[0])
        df = pd.concat([df, encoded_df], axis=1).drop([col_name], axis=1)
    print(df)
    return df


def get_highly_correlated_columns(df, display_corr=True):
    corrMatrix = df.corr().abs()
    upper = corrMatrix.where(np.triu(np.ones(corrMatrix.shape), k=1).astype(bool))
    to_drop = [column for column in upper.columns if any(upper[column] > 0.95) and column != 'Label']

    if display_corr:
        sn.heatmap(corrMatrix)
        plt.show()
        corrMatrix = df.corr().abs()
        sn.heatmap(corrMatrix)
        plt.show()
    return to_drop


def separate_attack_logs(df):
    attack_categories = df['attack_cat'].unique()
    for attack_cat in attack_categories:
        attack_rows = df[df['attack_cat'] == attack_cat]
        attack_rows.to_csv(f'./network_data/raw/attacks_separate/{attack_cat}.csv', index=False)


if __name__ == "__main__":
    df = load_data(DATA_PATH, FEATURES_PATH)
    df = process_missing_values(df)
    generate_class_plot(df, 'attack_cat')
    df = label_encoding(df, LABEL_ENCODING_COLUMN_NAMES)

    attack_dfs = []

    for attack_cat in ATTACK_CATEGORIES:
        if attack_cat != 'Normal':
            category_df = df[df['attack_cat'] == attack_cat]
            if len(category_df) > 1000:
                category_df = category_df[:1000]
            attack_dfs.append(category_df)
        else:
            normal_df = df[df['attack_cat'] == attack_cat]
            attack_dfs.append(normal_df.head(1000))  # get only 3000 of 'no attack' data

    df = pd.concat(attack_dfs)
    to_drop = get_highly_correlated_columns(df.head(100), display_corr=False)
    df.drop(to_drop, axis=1, inplace=True)

    ONE_HOT_ENCODING_COLUMN_NAMES = [col for col in ONE_HOT_ENCODING_COLUMN_NAMES if col in df.columns]
    NORMALIZATION_COLUMN_NAMES = [col for col in NORMALIZATION_COLUMN_NAMES if col in df.columns]

    df = one_hot_encoding(df, ONE_HOT_ENCODING_COLUMN_NAMES)
    scale_data(df, NORMALIZATION_COLUMN_NAMES)

    # to_drop = get_highly_correlated_columns(df[:100], display_corr=False)
    # df.drop(to_drop, axis=1, inplace=True)
    # df.to_csv(f'{PROCESSED_DATA_PATH}_JJJ', index=False)
    generate_class_plot(df, 'attack_cat')
    df.to_pickle(f"{PROCESSED_DATA_PATH}_pckl")

